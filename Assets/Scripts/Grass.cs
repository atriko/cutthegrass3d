﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grass : MonoBehaviour
{
    public GameObject pointText;
    Vector3 spawnPosition;
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Cutter")
        {
            spawnPosition = new Vector3(transform.position.x, transform.position.y + 1.8f, transform.position.z);
            Instantiate(pointText, spawnPosition, pointText.transform.rotation);
            Destroy(gameObject);
        }
    }
}
