﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointDestroyer : MonoBehaviour
{
    public static event Action pointScored = delegate { };

    private void Awake()
    {
        pointScored();
        Destroy(gameObject, 0.3f);
    }
}
