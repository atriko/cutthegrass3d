﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public static event Action outOfBounds = delegate { };
    public GameObject saw1;
    public GameObject saw2;
    Vector3 axis = new Vector3(0, 1, 0);
    Vector3 axisInverted = new Vector3(0, -1, 0);
    private float movementSpeed = 200f;
    private bool turnIt;
    private bool gameOver = false;
    public static bool gameStarted = false;

    // Start is called before the first frame update
    void Start()
    {
        GameManager.GameOver += EndGame;
    }

    private void EndGame()
    {
        gameOver = true;
        gameStarted = false;
    }
    private void Update()
    {
        
        if (Input.GetMouseButtonDown(0))
        {
            if (turnIt == false)
            {
                turnIt = true;
            }
            else
            {
                turnIt = false;
            }
        }
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        if (!gameOver && gameStarted)
        {
            if (turnIt == false) // turn by saw 2
            {
                if (saw2.transform.position.x < -4f || saw2.transform.position.x > 4f || saw2.transform.position.z < -11f || saw2.transform.position.z > -3f)
                {
                    outOfBounds();
                    return;
                }
                saw1.transform.SetParent(GameObject.FindGameObjectWithTag("Player").transform);
                saw2.transform.SetParent(null);
                Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, new Vector3(saw2.transform.position.x, Camera.main.transform.position.y, saw2.transform.position.z - 4f), Time.deltaTime * 2f);

                transform.RotateAround(saw2.transform.position, axis, Time.deltaTime * movementSpeed);

            }
            if (turnIt == true)
            {
                if (saw1.transform.position.x < -4f || saw1.transform.position.x > 4f || saw1.transform.position.z < -11f || saw1.transform.position.z > -3f)
                {
                    outOfBounds();
                    return;
                }
                saw2.transform.SetParent(GameObject.FindGameObjectWithTag("Player").transform);
                saw1.transform.SetParent(null);
                Camera.main.transform.position = Vector3.Lerp(Camera.main.transform.position, new Vector3(saw1.transform.position.x, Camera.main.transform.position.y, saw1.transform.position.z - 4f), Time.deltaTime * 2f);

                transform.RotateAround(saw1.transform.position, axisInverted, Time.deltaTime * movementSpeed);
            }
        }

    }
    private void OnDestroy()
    {
        GameManager.GameOver -= EndGame;

    }
}
