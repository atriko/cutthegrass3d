﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static event Action GameOver = delegate { };
    public GameObject levelClearCanvas;
    public GameObject gameOverCanvas;
    private void Awake()
    {
        Player.outOfBounds += FinishTheGame;
    }
    // Update is called once per frame
    void Update()
    {
        if (levelClearCanvas == null || gameOverCanvas == null)
        {
            levelClearCanvas = GameObject.FindGameObjectWithTag("LevelClearCanvas");
            gameOverCanvas = GameObject.FindGameObjectWithTag("GameOverCanvas");
        }
        if (!GameObject.FindGameObjectWithTag("Grass"))
        {
            LevelClear();
        }
                
    }
    private void FinishTheGame()
    {
        gameOverCanvas.SetActive(true);
        GameOver();
    }
    private void LevelClear()
    {
        levelClearCanvas.SetActive(true);
        GameOver();
    }
    private void OnDestroy()
    {
        Player.outOfBounds -= FinishTheGame;

    }
}
