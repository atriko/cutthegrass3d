﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    public TextMeshProUGUI scoreText;
    float score;
    // Start is called before the first frame update
    private void Start()
    {
        score = 0;
        PointDestroyer.pointScored += GivePoints;
    }

    private void GivePoints()
    {
        score += 1;
        scoreText.text = score.ToString();
    }
    
}
